/**
 * @author     Lukáš Materna
 * @copyright  2017 Lukáš Materna
 * @license    Licensed under {@link https://spdx.org/licenses/BSD-3-Clause-Clear.html BSD-3-Clause-Clear}.
 */

'use strict';
module.exports = function(app) {
    var todoList = require('../controllers/TodoController');

    // todoList Routes
    app.route('/tasks')
        .get(todoList.fetch_tasks)
        .post(todoList.create_task);

    app.route('/tasks/:taskId')
        .get(todoList.get_task)
        .put(todoList.update_task)
        .delete(todoList.delete_task);
};
