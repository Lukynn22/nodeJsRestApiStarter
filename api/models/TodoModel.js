/**
 * @author     Lukáš Materna
 * @copyright  2017 Lukáš Materna
 * @license    Licensed under {@link https://spdx.org/licenses/BSD-3-Clause-Clear.html BSD-3-Clause-Clear}.
 */

'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var TaskSchema = new Schema({
    name: {
        type: String,
        required: 'Name is required'
    },
    date_add: {
        type: Date,
        default: Date.now
    },
    status: {
        type: [{
            type: String,
            enum: ['waiting', 'working on it', 'done', 'completed']
        }],
        default: ['waiting']
    }
});
module.exports = mongoose.model('Tasks', TaskSchema);
