# Simple REST API Starter
Simple REST Api starter for NodeJS >= 8.9.x with MongoDB >= 4.x Using NPM >= 5.x

### Requiremenets
- npm 5.x
- NodeJS 8.9.x
- MongoDB 4.x
and higher...

### How to get it run?
```sh
$ npm install
$ npm run start
```
You should be available to access your REST API via address
```sh
localhost:3000
```

### License
[The Clear BSD License](https://spdx.org/licenses/BSD-3-Clause-Clear.html)
