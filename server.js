/**
 * @author     Lukáš Materna
 * @copyright  2017 Lukáš Materna
 * @license    Licensed under {@link https://spdx.org/licenses/BSD-3-Clause-Clear.html BSD-3-Clause-Clear}.
 */

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  bodyParser = require('body-parser');

// Load models
var Task = require('./api/models/TodoModel');

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/Tododb', { useMongoClient: true }); // Create MongoDB Connection

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Importing routes
var routes = require('./api/routes/TodoRoutes');
routes(app); // Register the route

// Handlig error 404
app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' was not found'})
});

app.listen(port);

console.log('Rest API started at localhost:' + port);
